<?php include_once 'Include/header.php';?>

<script type="text/javascript" >



</script>


<!-- Page Content -->
<div class="container">

	<div class="row">
		<!-- Blog Post Content Column -->
		<div class="col-lg-8">
            <?php if(!isset($blog)){
			echo '<h1> Create new blog post</h1>
                    <form role="form" action="'.base_url().'blog/save" method="POST">
                        <div class="form-group">
                            <label for="title">Title of the blog</label>
                            <input type="title" class="form-control" id="title" name="title" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="title">Category of your post</label>
                            <select class="form-control" name="category_id">
                                <option value="wildlife">Wildlife</option>
                                <option value="cars">Cars</option>
                                <option value="cricket">Cricket</option>
                                <option value="Citites">Cities</option>
                                <option value="Computer">Computer Science</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="content">Your Blog Content</label>
                            <textarea class="form-control" rows="8" name="content"></textarea>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>';
            }
            else{
//                $blog = $blog -> result();
               echo '<h1> Update blog post</h1>

                    <form role="form" action="'.base_url().'blog/save/'.$blog -> id.'" method="POST">
                        <div class="form-group">
                            <label for="title">Title of the blog</label>
                            <input type="title" class="form-control" id="title" name="title" value="'.$blog->title.'">
                        </div>
                        <div class="form-group">
                            <label for="title">Category of your post</label>
                            <select class="form-control" name="category_id">
                                <option value="wildlife">Wildlife</option>
                                <option value="cars">Cars</option>
                                <option value="cricket">Cricket</option>
                                <option value="Citites">Cities</option>
                                <option value="Computer">Computer Science</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="content">Your Blog Content</label>
                            <textarea class="form-control" rows="8" name="content">'.$blog->content.'</textarea>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>';
            }
            ?>
		</div>
	</div>
	<!-- /.row -->
</div>

<?php include_once 'Include/footer.php';?>