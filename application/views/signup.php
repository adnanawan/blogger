<?php include_once 'Include/header.php';?>

<div class="container">
    <h2>Sign Up</h2>
    <form name="signup" action="" method="post" role="form">
        <div class="form-group">
            <label for="email">Name:</label>
            <input type="email" class="form-control" style="width: 50%;" id="name" placeholder="Enter name">
        </div>
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" style="width: 50%;" id="email" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" style="width: 50%;" id="pwd" placeholder="Enter password">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>
<?php include_once 'Include/footer.php';?>
