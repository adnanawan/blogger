<!-- Blog Search Well -->

<div class="col-md-4">
    <div class="well">
        <h4>Blog Search</h4>
        <div class="input-group">
            <input type="text" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </span>
        </div>
        <!-- /.input-group -->
    </div>

    <!-- Blog Categories Well -->
    <div class="well">
        <h4>Blog Categories</h4>
        <div class="row">

            <div class="col-lg-6">
                <ul class="list-unstyled">
                    <li><a href="#">Computer Architecture</a>
                    </li>
                    <li><a href="#">Computer Networks</a>
                    </li>
                    <li><a href="#">Automata Theory </a>
                    </li>
                    <li><a href="#">Others</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-6">
                <ul class="list-unstyled">
                    <li><a href="#">Languages</a>
                    </li>
                    <li><a href="#">DataBase</a>
                    </li>
                    <li><a href="#">Maths</a>
                    </li>
                    <li><a href="#">More</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.row -->
    </div>

    <!--    Login            -->
    <div class="well">

        <div class="container">
            <h2>Login</h2>
            <form name="login" action="<?php echo base_url().'login'?>" method="post" role="form">
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" style="width: 25%;" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" id="pwd" style="width: 25%;" placeholder="Enter password">
                </div>
                <div class="checkbox">
                    <label><input type="checkbox"> Remember me</label>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>

    <!-- Side Widget Well -->
    <div class="well">
        <h4>Side Widget Well</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
    </div>

    </div>
</div>