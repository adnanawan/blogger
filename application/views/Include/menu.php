<div id="site_title"><h1><a href="#">Gray Box</a></h1></div>

<div id="tooplate_menu" class="ddsmoothmenu">
    <ul>
        <li><a href="<?php echo base_url().'home'; ?>" class="selected"><?php echo preg_replace('/\s+/', '', $menu_name[0]->name); ?> <span></span></a></li>
        <li><a href="<?php echo base_url().'categories'; ?>"><?php echo $menu_name[1]->name; ?><span></span></a>
            <ul>
                <li>
                    <a href="#">Languages</a>
                    <ul>
                        <li><a href="#">C++</a></li>
                        <li><a href="#">C#</a></li>
                        <li><a href="#">Java</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">DataBase</a>
                    <ul>
                        <li><a href="#">DBMS</a></li>
                        <li><a href="#">MySql</a></li>
                        <li><a href="#">Oracle</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Computer Architecture</a>
                    <ul>
                        <li><a href="#">DLD</a></li>
                        <li><a href="#">System Architecture</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Computer Networks</a>
                    <ul>
                        <li><a href="#">Computer Networks</a></li>
                        <li><a href="#">Advance Networking</a></li>
                    </ul>
                </li>
            </ul>
        </li>

        <li><a href="<?php echo base_url().'blog'; ?>"><?php echo $menu_name[2]->name; ?><span></span></a></li>
        <li><a href="<?php echo base_url().'signup'; ?>"><?php echo $menu_name[3]->name; ?><span></span></a></li>
        <li><a href="<?php echo base_url().'about'; ?>"><?php echo $menu_name[4]->name; ?><span></span></a></li>
        <li><a href="<?php echo base_url().'contact'; ?>"><?php echo $menu_name[5]->name; ?><span></span></a></li>

    </ul>


    <br style="clear: left" />
</div> <!-- end of tooplate_menu -->
</div> <!-- end of tooplate_header -->