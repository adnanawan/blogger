<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('user');
    }

    public function index(){
        $q = $this->user->get_blogs();
        $data['blogs'] = $q;
        $this->load->view('index', $data);
    }

}

?>