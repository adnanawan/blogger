<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('user');
    }

    function index()
    {
        $this->load->view('new_blog', $blog='');
    }

    public function save($id='') {
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('category_id', 'Category_id', 'required');
        $this->form_validation->set_rules('content', 'Content', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('blog');
        }
        else
        {
            $data = array(
                'title' => $this->input->post('title'),
                'category_id' => $this->input->post('category_id'),
                'content' => $this->input->post('content'),
                'created_at' => date('Y-m-d H:i:s')
            );
            if($id){
                $this->db->where('id', $id);
                $this->db->update('blogs', $data);
            }
            else{
                $this->db->insert('blogs', $data);
            }

            redirect('home');
        }
    }

    public function update($iD){

        $q["blog"] = $this -> user -> update_blog($iD);
        $this -> load -> view('new_blog', $q);

    }

    public function del($iD){
        $this -> user -> delete_blog($iD);
        redirect('home');
    }

}

?>